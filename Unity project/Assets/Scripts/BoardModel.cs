﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardModel {

	public static int MINES_COUNT = 10;

	Cell[,] m_cells;
	Vector2Int m_size;

	public void CreateNewModel(int rows, int columns) {
		m_cells = new Cell[rows, rows];
		m_size = new Vector2Int(columns, rows);


		InitializeRandomMines();

	}

	public void InitializeRandomMines() {
		int row;
		int column;
		for (int i = 0; i < MINES_COUNT; ++i) {
			do {
				row = Random.Range(0, m_size.y);
				column = Random.Range(0, m_size.x);
			} while (m_cells[row, column].IsMineCell); // for getting exactly 10 mines

			m_cells[row, column].IsMineCell = true;
			IncrementNeighbours(row, column);
		}
	}

	void IncrementNeighbours(int y, int x) {
		int minI = Mathf.Max(0, y - 1);
		int minJ = Mathf.Max(0, x - 1);
		int maxI = Mathf.Min(m_size.y, y + 2);
		int maxJ = Mathf.Min(m_size.x, x + 2);
		for (int i = minI; i < maxI; ++i) {
			for (int j = minJ; j < maxJ; ++j) {
				m_cells[i, j].IncrementNeighboursCount();
			}
		}
	}

	public Cell GetCellModel(int row, int column) {
		return m_cells[row, column];
	}
}
