﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardController {
	
	BoardModel m_boardModel = new BoardModel();
	BoardView m_boardView;

	int m_revealedCount;
	int m_winCondition;

	public delegate void OnGameOverDelegate(bool win);
	public event OnGameOverDelegate OnGameOverEvent;

	public BoardController() {
		m_boardView = GameObject.FindObjectOfType<BoardView>();
	}

	public void CreateNewBoard(int rows, int columns) {
		m_boardModel.CreateNewModel(rows, columns);
		m_boardView.CreateBoard(rows, columns);

		m_boardView.OnClickEvent += OnClickHandler;
		m_boardView.OnRevealEvent += OnRevealHandler;
		OnGameOverEvent += OnGameOverHandler;

		m_winCondition = rows * columns - BoardModel.MINES_COUNT;
	}

	public void OnClickHandler(int row, int column, System.Action<Cell> action) {
		var cell = m_boardModel.GetCellModel(row, column);
		action(cell);

		if (cell.IsMineCell) {
			if (OnGameOverEvent != null) {
				OnGameOverEvent(false);
			}
			return;
		}
	}

	void OnRevealHandler() {
		++m_revealedCount;
		Debug.Log(m_revealedCount);
		if (m_revealedCount == m_winCondition) {
			if (OnGameOverEvent != null) {
				OnGameOverEvent(true);
			}
		}
	}

	void OnGameOverHandler(bool win) {
		m_boardView.enabled = false; //to disable gameplay
	}
}
