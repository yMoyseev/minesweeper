﻿public struct Cell {
	
	public bool IsMineCell { get; set; }

	public int NeighboursCount { get; private set; }

	public void IncrementNeighboursCount() {
		++NeighboursCount;
	}
}
