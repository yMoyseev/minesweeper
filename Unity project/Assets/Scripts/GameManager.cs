﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

	public int m_Raws = 10;
	public int m_Columns = 10;

	BoardController m_controller;

	public UnityEngine.UI.Text m_GameOverText;

	void Start () {
		m_controller = new BoardController();
		m_controller.CreateNewBoard(m_Raws, m_Columns);

		m_controller.OnGameOverEvent += OnGameOverHandler;
	}

	void OnGameOverHandler(bool win) {
		if (win) {
			m_GameOverText.text = "YOU WIN";
		} else {
			m_GameOverText.text = "GAME OVER";
		}

		m_GameOverText.enabled = true;
	}
}
