﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardView : MonoBehaviour {

	public GameObject m_CellPrefab;

	public Sprite m_MineSprite;
	public Sprite m_ButtonSprite;
	public Sprite m_BackgroundSprite;

	Transform m_transform;
	Vector2Int m_size;

	SpriteRenderer[,] m_sprites;
	TextMesh[,] m_neighbours;

	// the struct for fast positions compairing
	struct ValuePair<T1, T2> {
		public T1 value1;
		public T2 value2;
		public ValuePair(T1 v1, T2 v2) {
			value1 = v1;
			value2 = v2;
		}
	}

	HashSet<ValuePair<int, int>> m_revealedPositions;

	public delegate void OnClickDelegate(int row, int column, System.Action<Cell> action);
	public event OnClickDelegate OnClickEvent;

	public delegate void OnRevealDelegate();
	public event OnRevealDelegate OnRevealEvent;

	void Awake() {
		m_transform = transform;
	}

	public void CreateBoard(int rows, int columns) {
		m_revealedPositions = new HashSet<ValuePair<int, int>>();
		m_size = new Vector2Int(columns, rows);
		m_sprites = new SpriteRenderer[rows, columns];
		m_neighbours = new TextMesh[rows, columns];
		float xStart = - rows / 2f;
		float yStart = columns / 2f;
		GameObject go;
		for (int i = 0; i < rows; ++i) {
			for (int j = 0; j < columns; ++j) {
				go = Instantiate(m_CellPrefab);
				go.transform.position = new Vector3(xStart + j, yStart - i, 0f);
				go.transform.parent = m_transform;
				go.name = "Cell " + i + "x" + j;
				m_sprites[i, j] = go.GetComponent<SpriteRenderer>();
				m_neighbours[i, j] = go.transform.GetComponentInChildren<TextMesh>();
			}
		}
	}

	void Update() {
		if (Input.GetMouseButtonUp(0)) {
			Click(Input.mousePosition);
		}
	}

	void Click(Vector3 position) {
		var worldPosition = Camera.main.ScreenToWorldPoint(position);
		int row = (int)(m_size.y / 2f - Mathf.Floor(worldPosition.y + .5f));
		int column = (int)(Mathf.Floor(worldPosition.x + .5f)  + m_size.x / 2f);

		if (row < 0 || column < 0 || row >= m_size.y || column >= m_size.x) {
			return;
		}

		if (OnClickEvent != null) {
			OnClickEvent(row, column, (c) => RevealCell(row, column, c));
		}
	}

	void RevealCell(int row, int column, Cell cell) {
		if (cell.IsMineCell) {
			m_sprites[row, column].sprite = m_MineSprite;
			return;
		}
		var vp = new ValuePair<int, int>(row, column);
		if (m_revealedPositions.Contains(vp)) {
			return;
		}
		m_sprites[row, column].sprite = m_BackgroundSprite;
		m_revealedPositions.Add(vp);
		if (OnRevealEvent != null) {
			OnRevealEvent();
		}

		if (cell.NeighboursCount > 0) {
			m_neighbours[row, column].text = cell.NeighboursCount.ToString();
		} else {
			PerformClickForNeighbours(row, column);
		}

	}

	void PerformClickForNeighbours(int y, int x) {
		int minI = Mathf.Max(0, y - 1);
		int minJ = Mathf.Max(0, x - 1);
		int maxI = Mathf.Min(m_size.y, y + 2);
		int maxJ = Mathf.Min(m_size.x, x + 2);
		ValuePair<int,int> vp;
		for (int i = minI; i < maxI; ++i) {
			for (int j = minJ; j < maxJ; ++j) {
				vp.value1 = i;
				vp.value2 = j;
				if (m_revealedPositions.Contains(vp)) {
					continue;
				}
				//m_revealedPositions.Add(vp);

				if (OnClickEvent != null) {
					OnClickEvent(i, j, (c) => RevealCell(i, j, c));
				}
			}
		}
	}
}
